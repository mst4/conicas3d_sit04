﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class clsCorrerTexto : MonoBehaviour {

    public Camera camaraHerramientas;
    public GameObject x;
    public GameObject y;
    [SerializeField]
    private RectTransform rectTransform;
    [SerializeField]
    private TextMeshProUGUI TxtCordenadas;
    [SerializeField]
    private GameObject laser;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!laser.activeSelf)
        {
            gameObject.SetActive(false);
        }

        var NuevaPosx = camaraHerramientas.WorldToScreenPoint(x.transform.position);
        var NuevaPosy = camaraHerramientas.WorldToScreenPoint(y.transform.position);

        rectTransform.position = new Vector3( NuevaPosy.x,NuevaPosx.y,NuevaPosy.z);
        //cordenadas.text = "[" + (X.cordenadaX * 100).ToString("0.00") + "," + (y.cordenadaY * 100).ToString("0.00") + "]";
        TxtCordenadas.text= "[" + (y.transform.position.x * 100).ToString("0.00") + "," + (x.transform.localPosition.z * 100).ToString("0.00") + "]";

    }







}
