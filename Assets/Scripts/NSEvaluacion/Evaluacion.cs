using NsSeguridad;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using System.Globalization;
using NSScore;

namespace NSEvaluacion
{
    public class Evaluacion : MonoBehaviour
    {
        #region members

        /// <summary>
        /// Rango de calificacion con posibilidad de definicion, ejemplo, hasta 5 cuando el array es definido con un solo elemento y este es numerico, ó de (5 - 10) 
        /// Cuando el array es definido con 2 elementos numericos, siendo el numero en el indice 0 la la nota mas baja y el numero en el indice 1 la nota mas alta 
        /// ó A - B - C - D - E si los elementos del array son definidos como letras, minimo 2 letras
        /// </summary>
        [SerializeField]
        private string[] rangoCalificacion;
        /// <summary>
        /// Rango de porcentaje para los intervalos
        /// </summary>
        [SerializeField]
        private float[] rangoPorcentaje5Letras;
        /// <summary>
        /// Clase que contiene las calificaciones de la situacion actualmente ejecutandose, esta clase se crea cada vez que se entra en una situación.
        /// </summary>
        [SerializeField]
        private CalificacionSituacion refCalificacionSituacion;
        /// <summary>
        /// Referencia a la clase seguridad para obtener la ruta del XML
        /// </summary>
        [SerializeField]
        private ClsSeguridad refClsSeguridad;
        [SerializeField]
        private clsEnvioPdf EnvioPdf;
        #endregion

        #region accesors

        public CalificacionSituacion _refCalificacionSituacion
        {
            get
            {
                return refCalificacionSituacion;
            }
        }
        #endregion

        #region monoBehaviour
        #endregion

        #region public methods

        /// <summary>
        /// Traduce la calificacion total de la situacion actual a la calificacion que le corresponde en el rango definido
        /// </summary>
        public string GetCalificacionTotalRango()
        {
            return FindObjectOfType<ScoreController>().GetScoreFromFactor01(refCalificacionSituacion.GetCalificacionTotal());
        }

        /// <summary>
        /// pasa la calificacion en unnumero de 0 a 1 donde 1 es el 100% y 0 el 0%
        /// </summary>
        public string GetCalificacionTotalNeta()
        {
            return refCalificacionSituacion.GetCalificacionTotal().ToString().Replace(",", ".");
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion para la seleccion de la funcion de graficado
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarFuncionGraficadoCorrecta(float argCalificacion)
        {
            refCalificacionSituacion._funcionGraficadoCorrecta = argCalificacion;
            Debug.Log("AsignarFuncionGraficadoCorrecta : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion para el registro de los datos
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarRegistroDatosCorrectos(float argCalificacion)
        {
            refCalificacionSituacion._registroDatosCorrectos = argCalificacion;
            Debug.Log("AsignarRegistroDatosCorrectos : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion de las preguntas respondidas 
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarPreguntasEvaluacionCorrectas(float argCalificacion)
        {
            refCalificacionSituacion._preguntasEvaluacionCorrectas = argCalificacion;
            Debug.Log("AsignarPreguntasEvaluacionCorrectas : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion con respecto a los intentos que a usado el usuario
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarCantidadIntentos(float argCalificacion)
        {
            refCalificacionSituacion._cantidadIntentos = argCalificacion;
            Debug.Log("AsignarCantidadIntentos : " + argCalificacion);
        }

        /// <summary>
        /// Crea un objeto de calificacion nuevo, esto debe usarse cada vez que el usuario selecciona una situacion
        /// </summary>
        public void NuevaCalificacion()
        {
            refCalificacionSituacion = new CalificacionSituacion()
            {
                _cantidadIntentos = 0,
                _funcionGraficadoCorrecta = 0,
                _preguntasEvaluacionCorrectas = 0,
                _registroDatosCorrectos = 0
            };
        }

        #endregion

        #region private methods 

        private void CargarNotasDesdeXML()
        {

#if !UNITY_WEBGL
            var tmpXmlDocument = new XmlDocument();

            try
            {
            tmpXmlDocument.Load(refClsSeguridad._fileName);
            var tmpListRangos = new List<string>();
           
            XmlNodeList tmpXmlNodeList = tmpXmlDocument.DocumentElement.GetElementsByTagName("rango");

            for (int i = 0; i < tmpXmlNodeList.Count; i++)
                tmpListRangos.Add(tmpXmlNodeList[i].InnerXml);

            if (tmpListRangos.Count > 0)
                rangoCalificacion = tmpListRangos.ToArray();

            Debug.LogError(rangoCalificacion.Length+ "length" + tmpXmlNodeList.Count);
            }catch (System.Exception e)
            {
                Debug.Log("No se encontro el archivo XML, se usaran las notas por default|| Excepcion : "+ e);
            }
#endif
        }
#endregion
    }
}