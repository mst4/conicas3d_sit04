﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NS 
{
	public class lineRender : MonoBehaviour
	{

        #region members

        [SerializeField]
        private LineRenderer mlineRender;

        private float pruebaFloat = 0.888888888f;

        private double pruebaDouble = 0.888888888888888888;

        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
            var tmpPosiciones = new Vector3[] { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(1, 0, 1) };
            mlineRender.SetPositions(tmpPosiciones);

            print("pruebaFloat : " + pruebaFloat);
            print("pruebaDouble : " + pruebaDouble);

            /*
                            1 = 500
             posicion actual laser = x
             ConversionCordenadasAUMA(0.5f) = posicion en ;
             ConversionCordenadasDesdeUMA(2000UMA) = posicion en unity;
             */
        }

        // Use this for initialization
        void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

 		#endregion

 		#region private methods

 		#endregion

 		#region public methods

 		#endregion

 		#region courutines

 		#endregion 
	}
}