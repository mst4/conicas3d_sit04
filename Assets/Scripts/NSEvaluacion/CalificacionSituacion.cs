﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEvaluacion
{
    /// <summary>
    /// Clase que contiene las calificaciones de las areas de una situacion
    /// </summary>
	public class CalificacionSituacion
    {
        /*Documentacion     
         * Todas las calificaciones van de 0 a 1, el metodo          
             */
        #region members

        /// <summary>
        /// Calificación de seleccion de la funcion de graficado correcta
        /// </summary>
        private float funcionGraficadoCorrecta;
        /// <summary>
        /// Calificación de los campos de registro de datos, cada campo tiene un peso en la calificacion, este peso es igual al resto de campos
        /// </summary>
        private float registroDatosCorrectos;
        /// <summary>
        /// Calificación de la evaluacion tipo PISA, cada pregunta tiene  4 posibles respuestas, solo una respuesta es la correcta, cada pregunta tiene un peso de calificacion igual al resto de preguntas
        /// </summary>
        private float preguntasEvaluacionCorrectas;
        /// <summary>
        /// Calificación de la cantidad de intentos del usuario, cada intento resta 10% a la calificacion
        /// </summary>
        private float cantidadIntentos = 10;


        #endregion

        #region accesors

        public float _funcionGraficadoCorrecta
        {
            set
            {
                funcionGraficadoCorrecta = value;
            }
        }

        public float _registroDatosCorrectos
        {
            set
            {
                registroDatosCorrectos = value;
            }
            get
            {//para habilitar el boton de reporte en el registro de datos
                return registroDatosCorrectos;
            }
        }

        public float _preguntasEvaluacionCorrectas
        {
            set
            {
                preguntasEvaluacionCorrectas = value;
            }
        }

        public float _cantidadIntentos
        {
            set
            {
                cantidadIntentos = value;
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Conseguir la calificacion total 0 = 0% , 1 = 100%
        /// </summary>
        /// <returns>Calificacion total basada en los porcentajes de peso para cada "area" dentro de la situacion, las "areas" son la seleccion de la funcion de graficado con un 30% de peso, los datos registrados para la funcion de graficado seleccionada  con un 30% de peso, respuestas a las preguntas de evaluacion con un 20% de peso y numero de intentos  con un 20% de peso.</returns>
        public float GetCalificacionTotal()
        {
            float aux;
            if (cantidadIntentos > 10)
                aux = 0;
            else
                aux = ((10-cantidadIntentos)/10);

            Debug.Log("cantidad de intentos "+cantidadIntentos+"este es el resultado "+ aux);
            return (funcionGraficadoCorrecta * 0.4f )+ (registroDatosCorrectos * 0.3f) +( preguntasEvaluacionCorrectas * 0.2f) + (aux * 0.1f);

        }
        #endregion

        #region courutines

        #endregion
    }
}