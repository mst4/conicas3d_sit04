﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class CuerpoCeleste : MonoBehaviour
	{
        #region members

        private bool ejecutarMovimiento = true;
        #endregion

        #region accesors

        public bool _ejecutarMovimiento
        {
            set
            {
                ejecutarMovimiento = value;
            }
        }
        #endregion

        #region public methods

        public void SetFuncionRutaParent(Transform argParent)
        {
            transform.SetParent(argParent);
        }

        public void EjecutarMovimientoCuerpoCelesteRutaCiclica(Vector3[] argPosicionesRutaXY, float argVelocidadObjetoOrbita = 0.025f, int argPosicionInicialRuta = 0)
        {
            StopAllCoroutines();
            StartCoroutine(CouMovimientoCuerpoCelesteRutaCiclica(argPosicionesRutaXY, argVelocidadObjetoOrbita, argPosicionInicialRuta));
        }

        public void EjecutarMovimientoCuerpoCelesteRutaPrincipioFin(Vector3[] argPosicionesRutaXYZ, float argVelocidadObjetoOrbita = 0.025f, int argPosicionInicialRuta = 0)
        {
            StopAllCoroutines();
            StartCoroutine(CouMovimientoCuerpoCelesteRutaPrincipioFin(argPosicionesRutaXYZ, argVelocidadObjetoOrbita, argPosicionInicialRuta));
        }
        #endregion

        #region courutines

        private IEnumerator CouMovimientoCuerpoCelesteRutaCiclica(Vector3[] argPosicionesRuta, float argVelocidadObjetoOrbita = 0.025f, int argPosicionInicialRuta = 0)
        {
            var tmpPosicionActualRuta = argPosicionInicialRuta;
            transform.localPosition = argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length];

            while (true)
            {
                yield return null;

                if (ejecutarMovimiento)
                {
                    var tmpNextPosition = argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length];

                    if (Vector3.Distance(transform.localPosition, tmpNextPosition) <= 0.005f)
                        tmpPosicionActualRuta++;
                
                    tmpNextPosition = argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length];
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, tmpNextPosition, argVelocidadObjetoOrbita * Time.deltaTime);                    
                }
            }
        }

        private IEnumerator CouMovimientoCuerpoCelesteRutaPrincipioFin(Vector3[] argPosicionesRuta, float argVelocidadObjetoOrbita = 0.025f, int argPosicionInicialRuta = 0)
        {            
            var tmpPosicionActualRuta = argPosicionInicialRuta;
            transform.localPosition = argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length];

            while (true)
            {
                yield return null;

                if (ejecutarMovimiento)
                {
                    if (Vector3.Distance(transform.localPosition, argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length]) <= 0.005f)
                    tmpPosicionActualRuta++;

                    if (tmpPosicionActualRuta % argPosicionesRuta.Length == 0)
                        transform.localPosition = argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length];

                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, argPosicionesRuta[tmpPosicionActualRuta % argPosicionesRuta.Length], argVelocidadObjetoOrbita * Time.deltaTime);
                }
            }
        }
        #endregion
    }
}