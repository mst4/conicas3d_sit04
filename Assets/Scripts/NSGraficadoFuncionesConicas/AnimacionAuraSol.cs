﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    public class AnimacionAuraSol : MonoBehaviour
    {
        #region members

        [SerializeField]
        private float velocidadRotacion;

        [SerializeField]
        private float velocidadAnimacionRayos;

        private float senoAnimacionRayos;

        [SerializeField]
        private Transform camaraObjetivo;
        #endregion

        #region monoBehaviour

        // Update is called once per frame
        void Update()
        {
            EjecutarAnimacion();
        }

        #endregion

        #region private methods

        private void EjecutarAnimacion()
        {
            transform.rotation = Quaternion.LookRotation(camaraObjetivo.position - transform.position, Vector3.forward);
            senoAnimacionRayos += Random.Range(-360, 360)*Time.deltaTime;
            //transform.localScale = Vector3.one * 0.4f + Vector3.one * Mathf.Sin(senoAnimacionRayos * Mathf.Deg2Rad) * 0.075f;
        }
        #endregion
    }
}