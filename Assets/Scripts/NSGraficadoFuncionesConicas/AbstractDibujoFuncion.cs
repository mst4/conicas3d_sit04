﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public abstract class AbstractDibujoFuncion : MonoBehaviour
	{
        #region members

        protected Vector3[] posicionesRutaXY;

        protected Vector3[] posicionesRutaXYZ;

        private LineRenderer lineRenderer;

        private bool dibujarFuncion;

        private float rangoPosicionesZ;

        #endregion

        #region accesors

        public Vector3[] _posicionesRutaXY
        {
            get
            {
                return posicionesRutaXY;
            }
        }

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

        #endregion

        #region private methods

        public virtual LineRenderer CrearLineRenderer(string argNombreFuncion, Material argMaterialDibujo, Transform argParent, bool argSobrePlanoCartesiano = true)
        {
            var tmpGameObjectLineRenderer = new GameObject("LineRender" + argNombreFuncion, typeof(LineRenderer));
            tmpGameObjectLineRenderer.transform.SetParent(argParent);

            if (argSobrePlanoCartesiano)
                tmpGameObjectLineRenderer.transform.localPosition = new Vector3(0, 0.1f, 0);
            else
                tmpGameObjectLineRenderer.transform.localPosition = Vector3.zero;

            tmpGameObjectLineRenderer.transform.localRotation = Quaternion.identity;
            tmpGameObjectLineRenderer.layer = LayerMask.NameToLayer("SistemaSolar");

            lineRenderer = tmpGameObjectLineRenderer.GetComponent<LineRenderer>();
            lineRenderer.widthMultiplier = 0.005f;
            lineRenderer.useWorldSpace = false;            
            lineRenderer.material = argMaterialDibujo;            

            return lineRenderer;
        }

        public virtual LineRenderer CrearLineRenderer(string argNombreFuncion, Transform argParent)
        {
            var tmpGameObjectLineRenderer = new GameObject("LineRender" + argNombreFuncion, typeof(LineRenderer));
            tmpGameObjectLineRenderer.transform.SetParent(argParent);
            tmpGameObjectLineRenderer.transform.localPosition = Vector3.zero;
            tmpGameObjectLineRenderer.transform.localRotation = Quaternion.identity;
            tmpGameObjectLineRenderer.layer = LayerMask.NameToLayer("SistemaSolar");

            lineRenderer = tmpGameObjectLineRenderer.GetComponent<LineRenderer>();
            lineRenderer.widthMultiplier = 0.005f;
            lineRenderer.useWorldSpace = false;

            return lineRenderer;
        }

        public Vector3[] GetCoordenadasXYZ(bool argSimularPositionY = false)
        {
            if (posicionesRutaXYZ != null)
                return posicionesRutaXYZ;

            posicionesRutaXYZ = new Vector3[posicionesRutaXY.Length];

            for (int i = 0; i < posicionesRutaXY.Length; i++)
            {
                posicionesRutaXYZ[i] = new Vector3(posicionesRutaXY[i][0], 0, posicionesRutaXY[i][1]);
            }

            return posicionesRutaXYZ;
        }
        
        #endregion

        #region public methods

        public abstract void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true);

        public abstract Vector3[] CalcularCordenadasFuncionMatematica();

        public CuerpoCeleste CrearObjetoOrbita(GameObject argPrefabObjetoOrbita, Transform argTransformLineRendererParent)
        {
            var tmpObjetoOrbita = Instantiate(argPrefabObjetoOrbita, argTransformLineRendererParent);
            tmpObjetoOrbita.layer = LayerMask.NameToLayer("SistemaSolar");
            return tmpObjetoOrbita.GetComponent<CuerpoCeleste>();
        }

        public void DestroyLineRenderer()
        {
            Destroy(lineRenderer.gameObject);
        }

        public void SetPositionLineRenderer(Vector3[] argPosition, bool argDibujarProgresivamente = true, float argDibujarFragmentoCada = 0.25f)
        {
            if (argDibujarProgresivamente)            
                StartCoroutine(CouSetPositionLineRenderer(argPosition, argDibujarFragmentoCada));            
            else
            {
                lineRenderer.positionCount = argPosition.Length;
                lineRenderer.SetPositions(argPosition);
            }
        }
        #endregion

        #region courutines

        private IEnumerator CouSetPositionLineRenderer(Vector3[] argPosition, float argDibujarFragmentoCada)
        {
            var tmpIndexPosition = 0;

            while (tmpIndexPosition < argPosition.Length)
            {
                tmpIndexPosition++;                
                lineRenderer.SetPosition(tmpIndexPosition, argPosition[tmpIndexPosition]);
                yield return new WaitForSeconds(argDibujarFragmentoCada);
            }
        }
        #endregion
    }
}