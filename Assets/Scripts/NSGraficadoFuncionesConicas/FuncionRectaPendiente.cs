﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionRectaPendiente : AbstractDibujoFuncion
	{

        #region members

        private float m;

        private float b;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        [SerializeField]
        private float velocidadObjetoOrbita = 0.25f;

        #endregion

        #region accesors

        public float _m
        {
            set
            {
                m = value;
            }
        }

        public float _b
        {
            set
            {
                b = value;
            }
        }
        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpX = -1; tmpX <= 1; tmpX += 0.025f)
            {
                var tmpPointY = m * tmpX + b;

                tmpPointY = Mathf.Clamp(tmpPointY, -1, 1);

                if (tmpPointY == -1 || tmpPointY == 1)
                    continue;

                tmpListaPosicionesGrafica.Add(new Vector2(tmpX, tmpPointY));
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {           
            CrearLineRenderer("LineaRectaPendiente", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

            /*var tmpPlanetaFoco = Instantiate(prefabPlanetaFoco, transform).GetComponent<Transform>();
            tmpPlanetaFoco.localPosition = focoParabola;*/

            var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
            tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);
        }

        
        #endregion

        #region courutines

        #endregion
    }
}