﻿using NSObjectSelector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clsHerramientaLaser : AbstractObjectSelectable
{


    #region members
    /// <summary>
    /// distancia maxima desde el centro a cual quier lado del plano
    /// </summary>
    public float posisionMaxima;
    /// <summary>
    /// numero de divisionesen el plano desde la mitad hacia un extremo
    /// </summary>
    public float NumeroDivisiones;

    /// <summary>
    /// codena par el usuario en el eje x
    /// </summary>
    public double cordenadaX;
    /// <summary>
    /// cordenada para el usuario en el eje y
    /// </summary>
    public double cordenadaY;

    public bool movX;

    public bool movY;

    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour

    void Awake()
    {

        isSelected = true;
    }

    void Start()
    {
        
    }

    private void Update()
    {
        if (movX&& posisionMaxima!=0) {
            cordenadaX = this.transform.localPosition.x * NumeroDivisiones/posisionMaxima;
        }
        if (movY&&posisionMaxima!=0) {
            cordenadaY = this.transform.localPosition.z * NumeroDivisiones/ posisionMaxima; ;
        }
    }
    #endregion

    #region private methods





    #endregion


    #region public methods

    public override void SetDraggedPosition(Vector3 argPosition, Vector3 argOffset)
    {
        var auxPos1 = argPosition;
        var auxPos2 = argOffset;

        if (movX&&!movY) {
            auxPos1 = new Vector3(argPosition.x, 0, 0);
            auxPos2 = new Vector3(argOffset.x, 0, 0);
        }
        if (!movX && movY)
        {
            auxPos1 = new Vector3(0,0,argPosition.z-argOffset.z);
            auxPos2 = new Vector3(0,0,argOffset.z - argOffset.z);

        }
        var resultado = auxPos1;//+ auxPos2;

        resultado[0] = Mathf.Clamp(resultado[0], -1, 1);
        resultado[2] = Mathf.Clamp(resultado[2], -1, 1);

        transform.localPosition = resultado;  

    }

    

    #endregion

    #region courutines

    #endregion







}
