﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfazAvanzada;
using NSSistemaSolar;
using NsSeguridad;

public class clsControlSegundaSala : MonoBehaviour {

    #region members
    //public GameObject TextUsirario;
    [SerializeField]
    private clsAdministradorSonido sonidos;

    public ClsInterfazSegundasala InterfazSegundaSala;
    public PanelInterfazBotonesInterfazAfuera interfazAfuera;
    public GameObject Joystick;
    public ControlCamaraPrincipal camaraPrincipal;
    public GameObject PadreConoIdle;
    public GameObject ConosDeLuzImagenesDeRender;
    public GameObject PLanoSegundaSala;
   // private ClsSeguridad seguridad;

    #endregion

    #region monoBehaviour
    // Use this for initialization
    void Start () {
        ActivarEjeciciosSegundaSala(false);
       // sonidos = GameObject.FindGameObjectWithTag("Sounds").GetComponent<clsAdministradorSonido>();
    }
	
	// Update is called once per frame
	void Update () {
       


    }

    #endregion


    #region private methods

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag== "Player")
        {
            Debug.Log("estoy dentro del colicionador segunda sala");
            ActivarEjeciciosSegundaSala(true);
            InterfazSegundaSala.ActivarPanel();
            interfazAfuera.ActivarPanel(false);
            Joystick.SetActive(false);
            camaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueMesaSegundaSala);
            sonidos.activarSOnidoConsola();
        }

    }

    #endregion


    #region public methods

    public void mtdSalir()
    {
        InterfazSegundaSala.ActivarPanel(false);
        interfazAfuera.ActivarPanel();
        Joystick.SetActive(true);
        camaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoquePrimeraPersona);
        ActivarEjeciciosSegundaSala(false);
        PLanoSegundaSala.SetActive(false);
        sonidos.activarSalirConsola();
    }


    public void ActivarEjeciciosSegundaSala(bool activar)
    {
        PLanoSegundaSala.SetActive(true);
        ConosDeLuzImagenesDeRender.SetActive(activar);
        PadreConoIdle.SetActive(!activar);
        
    }

    #endregion


}
