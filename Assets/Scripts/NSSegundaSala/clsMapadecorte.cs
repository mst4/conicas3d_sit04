﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSGraficadoFuncionesConicas;

public class clsMapadecorte : AbstractDibujoFuncion{

    public Transform[] puntosSectoprderecho;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, 8))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit");
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }


    }

    #region public methods

    public override Vector3[] CalcularCordenadasFuncionMatematica()
    {
        var tmpListaPosicionesGrafica = new List<Vector3>();


        posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
       return null;
    }

    public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
    {

       
        CrearLineRenderer("CorteDeLatorta", argMaterialDibujo, transform);
        SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

        /*var tmpPlanetaFoco = Instantiate(prefabPlanetaFoco, transform).GetComponent<Transform>();
        tmpPlanetaFoco.localPosition = focoParabola;*/

       // var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
       // tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);

    }

    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.tag);

       // collision.gameObject.
    }
    /*
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);

        
    }*/


}
