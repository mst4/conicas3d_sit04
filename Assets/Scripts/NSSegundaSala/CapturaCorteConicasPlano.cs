﻿using NSCreacionPDF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;
using NSTraduccionIdiomas;
namespace NSSegundaSala
{
    public class CapturaCorteConicasPlano : MonoBehaviour
    {

        #region members
        [SerializeField]
        private Camera cameraCapturaCorte;
        /// <summary>
        /// Render texture en donde quedan capturas las imagenes de cada hoja
        /// </summary>
        [SerializeField]
        private RenderTexture renderTexturePDF;       

        [SerializeField]
        private ControladorValoresPDFSegundaSala refControladorValoresPDFSegundaSala;

        [SerializeField]
        private BoxMessageManager mensajes;
        [SerializeField]
        private DiccionarioIdiomas dicIdiomas;
        #endregion

        #region monoBehaviour

        #endregion

        #region public methods

        public void CapturarCorteConicas()
        {
            
            StopAllCoroutines();
           
            StartCoroutine(CouCapturarCorteConicas());
        }

        #endregion

        #region private methods



        #endregion

        #region Courutines

        /// <summary>
        /// Courutina captura el corte de la conicas
        /// </summary>
        private IEnumerator CouCapturarCorteConicas()
        {
            cameraCapturaCorte.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            cameraCapturaCorte.Render();
            RenderTexture.active = renderTexturePDF;
            var tmpTexture2D = new Texture2D(1024, 512, TextureFormat.ARGB32, false, true);
            tmpTexture2D.ReadPixels(new Rect(448, 284, 1024, 512), 0, 0);
            tmpTexture2D.Apply();
            RenderTexture.active = null;           
            cameraCapturaCorte.gameObject.SetActive(false);
            refControladorValoresPDFSegundaSala.AddCapturaCorteConicas(tmpTexture2D);

        }

        #endregion
    }
}
