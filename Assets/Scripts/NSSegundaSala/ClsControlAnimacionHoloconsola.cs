﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSSistemaSolar;

public class ClsControlAnimacionHoloconsola : MonoBehaviour {
    public SistemaSolarManager MNGSistemaSolar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

        MNGSistemaSolar.setCambiarAccion(AccionesSistemaSolar.activado);
    }

    private void OnTriggerExit(Collider other)
    {
        MNGSistemaSolar.setCambiarAccion(AccionesSistemaSolar.desactivado);
    }

}
