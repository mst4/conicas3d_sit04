﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NsSeguridad;
using NSTraduccionIdiomas;
using NSBoxMessage;
using System;
using SimpleJSON;

namespace NSInterfazAvanzada 
{
	public class PanelInterfazLogin2Campos : AbstractPanelInterfaz
	{
        #region members
        public PanelInterfazMensaje Message;
        public InputField inputUsuario;
        public InputField inputPassword;
        public GameObject interfazAfuera;
        public Text[] PlaceholderLoguin2Campos;
        /// <summary>
        /// texto boton iniciar
        /// </summary>
        public TextParaTraducir BtIniciar;
        public Button Inicar;
        private DiccionarioIdiomas diccionario;
        private ClsSeguridad seguridad;
        private DiccionarioIdiomas Dic;
        [SerializeField]
        private BoxMessageManager refBoxMessageManager;

        private string[] cmd;
        private string correoMOno;
        private string nombreMono;
        private string instituMono;
        
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
            seguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
            diccionario = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
            Dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }

        // Use this for initialization
        void Start ()
		{
            seguridad.DlResLoginAula = mtdColback;
            PlaceholderLoguin2Campos[0].text= Dic.Traducir("TextPlaceholderUsuarioLoguin2campos", "");
            PlaceholderLoguin2Campos[1].text = Dic.Traducir("TextPlaceholderpasswordLoguin2campos", "");
            //inputUsuario.placeholder.GetComponent<Text>().text= Dic.Traducir("TextPlaceholderLoguin", "");
            if (seguridad.modoMonoUsuario)
            {

#if UNITY_EDITOR || UNITY_STANDALONE
                // Obtenemos los argumentos enviados a la aplicación de escritorio.
                cmd = System.Environment.CommandLine.Split(',');
                try
                {
                    if (cmd.Length > 5)
                    {
                        nombreMono = cmd[3];
                        instituMono = cmd[4];
                        correoMOno = cmd[5];

                    }                    
                    
                }
                catch
                {
                    refBoxMessageManager.MtdCreateBoxMessageInfo("esto es lo que hay dentro de la cmd "+cmd.ToString(),"ACEPTAR");
                }
#elif UNITY_ANDROID || UNITY_IPHONE
            try{
						AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
						AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
						if (currentActivity != null)
						{
                            AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
                            if (intent != null)
                                {
                                    nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                                    instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                                    correoMOno = safeCallStringMethod(intent, "getStringExtra", "correo");
                                }
                        }

				}catch (Exception e)
                {
					Debug.Log(e.ToString());
				}

#endif
                inputPassword.text = correoMOno;
            }


#if UNITY_WEBGL
            //CÓDIGO BASE_SITUATION
            //Url
            if (seguridad.ModoAula)
            {

                Debug.Log("entra Web aula");

                //CÓDIGO LOGUIN
                inputUsuario.enabled = false;
                inputPassword.enabled = false;

                string urlWebGL = seguridad.WebGLUrl;
                string webGLCode = seguridad.webGLCode;


                WWWForm form = new WWWForm();
                form.AddField("codigo", webGLCode);

                WWW wwwAulaWebGL = new WWW(urlWebGL, form);
                StartCoroutine(WaitForRequestAulaWebGL(wwwAulaWebGL));

            }
#endif

        }

        //rtrieving the data
        private IEnumerator WaitForRequestAulaWebGL(WWW www)
        {
            yield return www;
            if (www.error == null)
            {
                try
                {
                    Debug.Log(www.text);
                    var dataReceived = JSON.Parse(www.text);
                    Debug.Log("DataWebAULA: " + dataReceived); ;
                    if (dataReceived["RESULT"] != null)
                    {
                        inputUsuario.text = dataReceived["RESULT"]["email"].Value;
                        inputPassword.text = dataReceived["RESULT"]["password"].Value;
                    }
                }
                catch (Exception error)
                {
                    Debug.Log(error);
                }
            }
        }

        // Update is called once per frame
        void Update ()
		{
#if UNITY_EDITOR || UNITY_STANDALONE||UNITY_ANDROID || UNITY_IPHONE
            if (seguridad.modoMonoUsuario)
            {
                inputUsuario.text =  correoMOno;
            }/*else if(seguridad.ModoAula)
            {
                inputUsuario.text = "";
                inputPassword.text = "";
            }*/
#endif

        }

#endregion

#region private methods

        /// <summary>
        /// metodo que se agrega al delegado de seguridad para saber la respuesta del la operacion loguin
        /// </summary>
        /// <param name="op"></param>
        private void mtdColback(int op) {
            if (op==1)
            {
                ActivarPanel(false);
                interfazAfuera.GetComponent<PanelInterfazBotonesInterfazAfuera>().ActivarPanel(true);
            }
            else {
                if (op==0)
                {
                    BtIniciar.text = diccionario.Traducir("TextBotonIngresar", "INGRESAR");
                    Inicar.enabled = true;
                }
            }

        }

#endregion

#region public methods

        /// <summary>
        /// metodo que verifica campos de loguin y pasa los datos a seguridad
        /// </summary>
        public void MtdBtnIniciarAplicacion()
        {


            if (inputUsuario.text != "" && inputPassword.text != "")
            {
                Debug.Log("entre a llamar aula loginRequest");
                seguridad.mtdLoguinAula(inputUsuario.text, inputPassword.text);
                BtIniciar.text = diccionario.Traducir("BotonLoguinCargando", "cargando..");
                Inicar.enabled = false;
                //diccionario.Traducir(BtIniciar);
            }
            else
            {

                //Message.MtdActivarMnesaje("Todos los campos son requeridos");
                refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeLLenarcampos", "Todos los campos son requeridos."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
            }
        }

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

#endregion

#region courutines

#endregion

        public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
        {

				if (args == null) args = new object[] { null };
				IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
				jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);
				try
				{
					IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
					if (IntPtr.Zero != returnValue)
					{
						var val = AndroidJNI.GetStringUTFChars(returnValue);
						AndroidJNI.DeleteLocalRef(returnValue);
						return val;
					}
				}
				finally
				{
					AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
				}

				return null;

        }
    }
}