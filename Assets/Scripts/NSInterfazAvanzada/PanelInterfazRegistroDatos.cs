﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSCreacionPDF;
using NSRegistroDatos;
using UnityEngine.UI;
using NSGraficadoFuncionesConicas.FuncionAleatoria;
using NSBoxMessage;
using NSEvaluacion;
using NSGraficadoFuncionesConicas;
using NSTraduccionIdiomas;

namespace NSInterfazAvanzada
{
    public class PanelInterfazRegistroDatos : AbstractPanelInterfaz
    {
        #region members

        [SerializeField]
        private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField]
        private CapturaFuncionGraficada refCapturaFuncionGraficada;

        [SerializeField]
        private PanelInterfazBotonesSeleccionSituacion refPanelInterfazBotonesSeleccionSituacion;

        [SerializeField]
        private ControladorRegistroDatos refControladorRegistroDatos;

        [SerializeField]
        private BoxMessageManager refBoxMessageManager;

        [SerializeField]
        private Button buttonReporte;

        [SerializeField, Header("Inputs Recta"), Space(10)]
        private InputField inputFieldRectaPuntoXTrayectoria1;

        [SerializeField]
        private InputField inputFieldRectaPuntoYTrayectoria1;

        [SerializeField]
        private InputField inputFieldRectaPuntoXTrayectoria2;

        [SerializeField]
        private InputField inputFieldRectaPuntoYTrayectoria2;

        [SerializeField, Header("Inputs Parabola"), Space(10)]
        private ToggleGroup toggleGroupDireccionParabola;

        [SerializeField]
        private InputField inputFieldParabolaPuntoXTrayectoria;

        [SerializeField]
        private InputField inputFieldParabolaPuntoYTrayectoria;

        [SerializeField, Header("Inputs Circunferencia"), Space(10)]
        private InputField inputFieldCircunferenciaDiametro;

        [SerializeField]
        private InputField inputFieldCircunferenciaPerimetro;

        [SerializeField, Header("Inputs Elipse"), Space(10)]
        private InputField inputFieldElipseExcentricidad;

        [SerializeField]
        private InputField inputFieldElipseTiempoTranslacion;

        [SerializeField, Header("Inputs Hiperbola"), Space(10)]
        private ToggleGroup toggleGroupOrientacionHiperbola;

        [SerializeField]
        private InputField inputFieldHiperbolaExcentricidad;

        [SerializeField]
        private Evaluacion refEvaluacion;

        /// <summary>
        /// Para saber cuantos campos del registro estan mal
        /// </summary>
        private float cantidadCamposCorrectos;

        /// <summary>
        /// Para saber la cantidad de campos en una verificacion y asi poder sacar la calificacion
        /// </summary>
        private float cantidadCamposVerificados;

        /// <summary>
        /// Para ocultar esta interfaz cuando se selecciona el boton terminar y se muestra la ventana de evaluacion
        /// </summary>
        [SerializeField]
        private PanelInterfazPlano refPanelInterfazPlano;

        /// <summary>
        /// Padre de todos los formularios de registros de datos
        /// </summary>
        [SerializeField, Header("Reporte PDF"), Space(10)]
        private GameObject inputsRegistroDatos;

        [SerializeField]
        private ControladorValoresPDF refControladorValoresPDF;
        [SerializeField]
        private ControladorDatosSesion controladorSesion;

        private DiccionarioIdiomas DicIdiomas;
        #endregion

        private void OnEnable()
        {
            HabilitarBotonReporte();
        }
        private void Awake()
        {
            DicIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }


        #region private methods

        private float ConvertirEscala(float argValorConvertir)
        {
            return argValorConvertir / 100f;
        }

        private void VerificarDatosRecta()
        {
            var tmpDatosRegistrados = new DatosRegistroRecta()
            {
                puntoSobreTrayectoria1 = new Vector2(ConvertirEscala(float.Parse(inputFieldRectaPuntoXTrayectoria1.text)), ConvertirEscala(float.Parse(inputFieldRectaPuntoYTrayectoria1.text))),
                puntoSobreTrayectoria2 = new Vector2(ConvertirEscala(float.Parse(inputFieldRectaPuntoXTrayectoria2.text)), ConvertirEscala(float.Parse(inputFieldRectaPuntoYTrayectoria2.text)))
            };

            var tmpDatosSimulador = new DatosRectaSimulador()
            {
                b = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaRectaSeleccionada._b,
                m = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaRectaSeleccionada._m
            };

            cantidadCamposVerificados = 2;

            var tmpDatosIcorrectos = refControladorRegistroDatos.VerificarDatosRecta(tmpDatosRegistrados, tmpDatosSimulador);

            if (tmpDatosRegistrados.puntoSobreTrayectoria1 == tmpDatosRegistrados.puntoSobreTrayectoria2)
            {
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajePuntosEntrayectoria", "Los puntos sobre la trayectoria no pueden ser iguales."), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria1, tmpDatosIcorrectos[0]);
                ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria1, tmpDatosIcorrectos[0]);
                ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria2, true);
                ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria2, true);

                if (!tmpDatosIcorrectos[0])
                    cantidadCamposCorrectos++;
            }
            else
            {
                ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria1, tmpDatosIcorrectos[0]);
                ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria1, tmpDatosIcorrectos[0]);
                ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria2, tmpDatosIcorrectos[1]);
                ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria2, tmpDatosIcorrectos[1]);

                if (!tmpDatosIcorrectos[0])
                    cantidadCamposCorrectos++;

                if (!tmpDatosIcorrectos[1])
                    cantidadCamposCorrectos++;

                if (cantidadCamposCorrectos != 2)                
                    refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));                
                else                
                    refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosCorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));                
            }
        }

        private void VerificarDatosParabola()
        {
            DireccionParabola tmpDireccionParabola = DireccionParabola.noDefinido;

            var tmpToggleDireccionSeleccionado = -1;

            for (int i = 0; i < toggleGroupDireccionParabola.transform.childCount; i++)
            {
                if (toggleGroupDireccionParabola.transform.GetChild(i).GetComponent<Toggle>().isOn)
                {
                    if (i == 0)
                        tmpDireccionParabola = DireccionParabola.arriba;
                    else if (i == 1)
                        tmpDireccionParabola = DireccionParabola.abajo;
                    else if (i == 2)
                        tmpDireccionParabola = DireccionParabola.izquierda;
                    else if (i == 3)
                        tmpDireccionParabola = DireccionParabola.derecha;

                    tmpToggleDireccionSeleccionado = i;
                    break;
                }
            }
                
            var tmpDatosRegistrados = new DatosRegistroParabola()
            {
                direccionParabola = tmpDireccionParabola,
                puntoSobreTrayectoria = new Vector2(ConvertirEscala(float.Parse(inputFieldParabolaPuntoXTrayectoria.text)), ConvertirEscala(float.Parse(inputFieldParabolaPuntoYTrayectoria.text)))
            };

            var tmpDatosSimulador = new DatosParabolaSimulador()
            {
                direccionParabola = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._direccionParabola,
                h = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._h,
                k = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._k,
                p = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._p
            };

            var tmpDatosIcorrectos = refControladorRegistroDatos.VerificarDatosParabola(tmpDatosRegistrados, tmpDatosSimulador);

            if (tmpToggleDireccionSeleccionado != -1)
                ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(tmpToggleDireccionSeleccionado).GetComponent<Toggle>(), tmpDatosIcorrectos[0]);

            ActivarImagenErrorInput(inputFieldParabolaPuntoXTrayectoria, tmpDatosIcorrectos[1]);
            ActivarImagenErrorInput(inputFieldParabolaPuntoYTrayectoria, tmpDatosIcorrectos[1]);

            cantidadCamposVerificados = 2;

            if (!tmpDatosIcorrectos[0])
                cantidadCamposCorrectos++;

            if (!tmpDatosIcorrectos[1])
                cantidadCamposCorrectos++;

            if (cantidadCamposCorrectos != 2)
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
            else
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosCorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }

        private void VerificarDatosCircunferencia()
        {
            var tmpDatosRegistrados = new DatosRegistroCircunferencia()
            {
                formaCircunferencia = FormaCircunferencia.circular,
                diametro = ConvertirEscala(float.Parse(inputFieldCircunferenciaDiametro.text)),
                perimetro = ConvertirEscala(float.Parse(inputFieldCircunferenciaPerimetro.text))
            };

            var tmpDatosSimulador = new DatosCircunferenciaSimulador()
            {
                h = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaCircunferenciaSeleccionada._h,
                k = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaCircunferenciaSeleccionada._k,
                r = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaCircunferenciaSeleccionada._r
            };

            var tmpDatosIcorrectos = refControladorRegistroDatos.VerificarDatosCircunferencia(tmpDatosRegistrados, tmpDatosSimulador);
            ActivarImagenErrorInput(inputFieldCircunferenciaDiametro, tmpDatosIcorrectos[0]);
            ActivarImagenErrorInput(inputFieldCircunferenciaPerimetro, tmpDatosIcorrectos[1]);

            cantidadCamposVerificados = 2;

            if (!tmpDatosIcorrectos[0])
                cantidadCamposCorrectos++;

            if (!tmpDatosIcorrectos[1])
                cantidadCamposCorrectos++;

            if (cantidadCamposCorrectos != 2)
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
            else
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosCorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }

        private void VerificarDatosElipse()
        {
            var tmpDatosRegistrados = new DatosRegistroElipse()
            {
                excentricidad = ConvertirEscala(float.Parse(inputFieldElipseExcentricidad.text)),
                tiempoTranslacion = ConvertirEscala(float.Parse(inputFieldElipseTiempoTranslacion.text))
            };

            var tmpDatosSimulador = new DatosElipseSimulador()
            {
                h = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaElipseSeleccionada._h,
                k = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaElipseSeleccionada._k,
                a = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaElipseSeleccionada._a,
                b = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaElipseSeleccionada._b
            };

            var tmpDatosIcorrectos = refControladorRegistroDatos.VerificarDatosElipse(tmpDatosRegistrados, tmpDatosSimulador);
            ActivarImagenErrorInput(inputFieldElipseExcentricidad, tmpDatosIcorrectos[0]);
            ActivarImagenErrorInput(inputFieldElipseTiempoTranslacion, tmpDatosIcorrectos[1]);

            cantidadCamposVerificados = 2;

            if (!tmpDatosIcorrectos[0])
                cantidadCamposCorrectos++;

            if (!tmpDatosIcorrectos[1])
                cantidadCamposCorrectos++;

            if (cantidadCamposCorrectos != 2)
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
            else
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosCorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }

        private void VerificarDatosHiperbola()
        {
            SentidoHiperbola tmpSentidoHiperbola = SentidoHiperbola.noDefinido;
            var tmpToggleOrientacionSeleccionado = -1;

            for (int i = 0; i < toggleGroupOrientacionHiperbola.transform.childCount; i++)
            {
                if (toggleGroupOrientacionHiperbola.transform.GetChild(i).GetComponent<Toggle>().isOn)
                {
                    if (i == 0)
                        tmpSentidoHiperbola = SentidoHiperbola.vertical;
                    else if (i == 1)
                        tmpSentidoHiperbola = SentidoHiperbola.horizontal;

                    tmpToggleOrientacionSeleccionado = i;
                    break;
                }

            }

            var tmpDatosRegistrados = new DatosRegistroHiperbola()
            {
                sentidoHiperbola = tmpSentidoHiperbola,
                excentricidad = float.Parse(inputFieldHiperbolaExcentricidad.text)//ConvertirEscala(float.Parse(inputFieldHiperbolaExcentricidad.text))
            };

            var tmpDatosSimulador = new DatosHiperbolaSimulador()
            {
                sentidoHiperbola = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._sentidoHiperbola,
                h = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._h,
                k = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._k,
                a = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._a,
                b = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._b
            };

            var tmpDatosIcorrectos = refControladorRegistroDatos.VerificarDatosHiperbola(tmpDatosRegistrados, tmpDatosSimulador);

            if (tmpToggleOrientacionSeleccionado != -1)
                ActivarImagenErrorToggle(toggleGroupOrientacionHiperbola.transform.GetChild(tmpToggleOrientacionSeleccionado).GetComponent<Toggle>(), tmpDatosIcorrectos[1]);

            ActivarImagenErrorInput(inputFieldHiperbolaExcentricidad, tmpDatosIcorrectos[0]);

            cantidadCamposVerificados = 2;

            if (!tmpDatosIcorrectos[0])
                cantidadCamposCorrectos++;

            if (!tmpDatosIcorrectos[1])
                cantidadCamposCorrectos++;

            if (cantidadCamposCorrectos != 2)
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
            else
                refBoxMessageManager.MtdCreateBoxMessageInfo(DicIdiomas.Traducir("mensajeDatosIngresadosCorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }

        private void ActivarPanelContenedorRegistros()
        {
            var tmpPadrePanelesContenedores = transform.Find("ImageFondoInputs");

            for (int i = 0; i < tmpPadrePanelesContenedores.childCount; i++)
                tmpPadrePanelesContenedores.GetChild(i).gameObject.SetActive(false);

            switch (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada)
            {
                case SituacionSeleccionada.recta:
                    tmpPadrePanelesContenedores.Find("PanelContenedorRegistrosLineaRecta").gameObject.SetActive(true);
                    break;

                case SituacionSeleccionada.parabola:
                    tmpPadrePanelesContenedores.Find("PanelContenedorRegistrosParabola").gameObject.SetActive(true);
                    break;

                case SituacionSeleccionada.circunferencia:
                    tmpPadrePanelesContenedores.Find("PanelContenedorRegistrosCircunferencia").gameObject.SetActive(true);
                    break;

                case SituacionSeleccionada.elipse:
                    tmpPadrePanelesContenedores.Find("PanelContenedorRegistrosElipse").gameObject.SetActive(true);
                    break;

                case SituacionSeleccionada.hiperbola:
                    tmpPadrePanelesContenedores.Find("PanelContenedorRegistrosHiperbola").gameObject.SetActive(true);
                    break;
            }
        }

        private void ActivarImagenErrorToggle(Toggle argInputField, bool argActivar = true)
        {
            argInputField.transform.Find("ImageDireccionCorrectaIncorrecta").gameObject.SetActive(argActivar);
        }

        private void ActivarImagenErrorInput(InputField argInputField, bool argActivar = true)
        {
            argInputField.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(argActivar);
        }

        private void HabilitarBotonReporte()
        {
            if (refEvaluacion._refCalificacionSituacion._registroDatosCorrectos == 0)
                buttonReporte.interactable = false;
            else
                buttonReporte.interactable = true;
        }

        private void GenerarReporte()
        {
            ActivarPanel(false);
            refPanelInterfazEvaluacion.ActivarPanel();
            refPanelInterfazPlano.ActivarPanel(false);
            refControladorValoresPDF.SetInputsRegistroDatos(inputsRegistroDatos);
        }

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            if (argActivar)
            {
                refCapturaFuncionGraficada.CapturarFuncionGraficada();
                ActivarPanelContenedorRegistros();
            }

            Mostrar(argActivar);
        }

        public void OnButtonReporte()
        {
            if (cantidadCamposCorrectos < cantidadCamposVerificados)
                refBoxMessageManager.MtdCreateBoxMessageDecision(DicIdiomas.Traducir("mensajeCamposResgistroIncorrectos", "test"), DicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"), DicIdiomas.Traducir("TextBotonCancelar", "CANCELAR"), GenerarReporte);
            else
                GenerarReporte();
        }

        public void OnButtonVerificar()
        {
            cantidadCamposCorrectos = 0;
            //LimpiarErroresVerificacion();

            switch (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada)
            {
                case SituacionSeleccionada.recta:
                    VerificarDatosRecta();
                    break;

                case SituacionSeleccionada.parabola:
                    VerificarDatosParabola();
                    break;

                case SituacionSeleccionada.circunferencia:
                    VerificarDatosCircunferencia();
                    break;

                case SituacionSeleccionada.elipse:
                    VerificarDatosElipse();
                    break;

                case SituacionSeleccionada.hiperbola:
                    VerificarDatosHiperbola();
                    break;
            }

            refEvaluacion.AsignarRegistroDatosCorrectos((1f / cantidadCamposVerificados) * cantidadCamposCorrectos);
            var calificacionActual = refEvaluacion._refCalificacionSituacion;

            if(calificacionActual._registroDatosCorrectos!=1f)
                controladorSesion.AddIntentos();


            buttonReporte.interactable = true;
        }

        /// <summary>
        /// Asigna a todos los inputs field el valor de 0 por defecto y oculta las X de error
        /// </summary>
        public void PrepararInputsFieldNuevaSituacion()
        {
            //Inputs Recta
            inputFieldRectaPuntoXTrayectoria1.text = "0";
            inputFieldRectaPuntoYTrayectoria1.text = "0";
            inputFieldRectaPuntoXTrayectoria2.text = "0";
            inputFieldRectaPuntoYTrayectoria2.text = "0";

            ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria1, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria1, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria2, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria2, false);

            //Inputs parabola
            toggleGroupDireccionParabola.SetAllTogglesOff();
            inputFieldParabolaPuntoXTrayectoria.text = "0";
            inputFieldParabolaPuntoYTrayectoria.text = "0";

            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(0).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(1).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(2).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(3).GetComponent<Toggle>(), false);

            ActivarImagenErrorInput(inputFieldParabolaPuntoXTrayectoria, false);
            ActivarImagenErrorInput(inputFieldParabolaPuntoYTrayectoria, false);

            //Inputs circunferencia
            inputFieldCircunferenciaDiametro.text = "0";
            inputFieldCircunferenciaPerimetro.text = "0";

            ActivarImagenErrorInput(inputFieldCircunferenciaDiametro, false);
            ActivarImagenErrorInput(inputFieldCircunferenciaDiametro, false);

            //Inputs elipse
            inputFieldElipseExcentricidad.text = "0";
            inputFieldElipseTiempoTranslacion.text = "0";

            ActivarImagenErrorInput(inputFieldElipseExcentricidad, false);
            ActivarImagenErrorInput(inputFieldElipseTiempoTranslacion, false);

            //Input Hiperbola
            toggleGroupOrientacionHiperbola.SetAllTogglesOff();
            inputFieldHiperbolaExcentricidad.text = "0";

            ActivarImagenErrorInput(inputFieldHiperbolaExcentricidad, false);
        }

        private void LimpiarErroresVerificacion()
        {
            //Inputs Recta
            ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria1, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria1, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoXTrayectoria2, false);
            ActivarImagenErrorInput(inputFieldRectaPuntoYTrayectoria2, false);

            //Inputs parabola
            toggleGroupDireccionParabola.SetAllTogglesOff();

            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(0).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(1).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(2).GetComponent<Toggle>(), false);
            ActivarImagenErrorToggle(toggleGroupDireccionParabola.transform.GetChild(3).GetComponent<Toggle>(), false);

            ActivarImagenErrorInput(inputFieldParabolaPuntoXTrayectoria, false);
            ActivarImagenErrorInput(inputFieldParabolaPuntoYTrayectoria, false);

            //Inputs circunferencia
            ActivarImagenErrorInput(inputFieldCircunferenciaDiametro, false);
            ActivarImagenErrorInput(inputFieldCircunferenciaDiametro, false);

            //Inputs elipse
            ActivarImagenErrorInput(inputFieldElipseExcentricidad, false);
            ActivarImagenErrorInput(inputFieldElipseTiempoTranslacion, false);

            //Input Hiperbola
            toggleGroupOrientacionHiperbola.SetAllTogglesOff();

            ActivarImagenErrorInput(inputFieldHiperbolaExcentricidad, false);
        }


        #endregion
    }
}