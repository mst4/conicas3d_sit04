﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSGraficadoFuncionesConicas;
using NSGraficadoFuncionesConicas.FuncionAleatoria;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{
    public class PanelInterfazSeleccionFuncion : AbstractPanelInterfaz
    {


        #region members
        private bool activoBotonVerificar; 

        [SerializeField]
        private GameObject verificarEcuacion;
        [SerializeField]
        private Sprite BotonVerificarEcuacionApagado;
        [SerializeField]
        private Sprite BotonVerificarEcuacionEnsendido;

        [SerializeField]
        private GameObject ImageContenedorFuncionSeleccionada;

        [SerializeField]
        private Button buttonElegir;

        private int indiceFuncionSeleccionada = -1;

        private int indiceFuncionSeleccionadaTemporal = -1;

        [SerializeField]
        private PlanoCartesiano refPlanoCartesiano;

        [SerializeField]
        private Transform transformSistemaSolar;

        private GameObject ultimaFuncionVerificada;

        [SerializeField]
        private PanelInterfazBotonesSeleccionSituacion refPanelInterfazBotonesSeleccionSituacion;

        private byte cantidadErrores;

        [SerializeField]
        private PanelInterfazBotonesIzquierda refPanelInterfazBotonesIzquierda;

        [SerializeField]
        private BoxMessageManager refBoxMessageManager;

        [SerializeField]
        private ControladorValoresPDF refControladorValoresPDF;

        /// <summary>
        /// para agregar intentos de verificacion
        /// </summary>
        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField]
        private Evaluacion refEvaluacion;

        [SerializeField, Header("Resultado grafica PDF"), Space(10)]
        private Image imageResultado;

        [SerializeField, Header("Margenes error"), Space]
        private float margenErrorLineaRecta = 0.025f;

        [SerializeField]
        private float margenErrorParabola = 0.025f;

        [SerializeField]
        private float margenErrorCircunferencia = 0.025f;

        [SerializeField]
        private float margenErrorElipse = 0.025f;

        [SerializeField]
        private float margenErrorHiperbola = 2f;
        #endregion

        #region accesors

        #endregion

        #region monoBehaviour
        private void Update()
        {
            if (activoBotonVerificar)
            {
                verificarEcuacion.GetComponent<Button>().interactable = true;
                verificarEcuacion.GetComponent<Image>().sprite = BotonVerificarEcuacionEnsendido;
            }
            else
            {
                verificarEcuacion.GetComponent<Button>().interactable = false;
                verificarEcuacion.GetComponent<Image>().sprite = BotonVerificarEcuacionApagado;
            }

        }
        #endregion

        #region private methods

        private float ConvertirEscala(float argValorConvertir)
        {
            return argValorConvertir / 100f;
        }

        private bool GetErrorValor(float argValorEsperado, float argValorAsignado, float argMargenError)
        {
            var tmpError = Mathf.Abs(argValorEsperado - argValorAsignado) >= argMargenError;

            if (tmpError)
                cantidadErrores++;

            return tmpError;
        }

        private void ResetearElementosUsados()
        {
            var tmpTransformRegistroDatosInputs = transform.Find("PanelRegistroDatosInputs");

            for (int i = 1; i < tmpTransformRegistroDatosInputs.childCount; i++)
                for (int j = 0; j < tmpTransformRegistroDatosInputs.GetChild(i).childCount; j++)
                {
                    var tmpContenedorInputsField = tmpTransformRegistroDatosInputs.GetChild(i).GetChild(j);

                    var tmpInputField = tmpContenedorInputsField.GetChild(1).GetComponent<InputField>();

                    if (tmpInputField)
                    {
                        tmpInputField.text = "0";
                        var tmpImageValorFuncionIncorrecto = tmpInputField.transform.Find("ImageValorFuncionIncorrecto");

                        if (tmpImageValorFuncionIncorrecto != null)
                            tmpImageValorFuncionIncorrecto.gameObject.SetActive(false);
                    }

                    tmpTransformRegistroDatosInputs.GetChild(i).gameObject.SetActive(false);
                }

            LimpiarXSeleccionfuncion();
        }

        private void LimpiarXSeleccionfuncion()
        {
            var tmpPanelContenedorListaFunciones = transform.Find("PanelContenedorListaFunciones");

            for (int i = 0; i < tmpPanelContenedorListaFunciones.childCount; i++)
            {
                var tmpBotonFuncion = tmpPanelContenedorListaFunciones.GetChild(i).GetComponent<Button>();
                tmpBotonFuncion.interactable = true;
                tmpBotonFuncion.transform.Find("ImageFuncionSeleccionadaIncorrecta").gameObject.SetActive(false);
            }

            indiceFuncionSeleccionada = -1;
        }

        private float VerificarDatosRecta(LineRenderer argLineRender, InputField argInputFieldM, InputField argInputFieldB)
        {
            var tmpNotaMaxima = 0f;
            var tmpFragmentoNota = 1f / 2f;
            var tmpFuncionAleatoriaSeleccionada = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaRectaSeleccionada;

            var tmpM = float.Parse(argInputFieldM.text);
            var tmpB = ConvertirEscala(float.Parse(argInputFieldB.text));

            var tmpMError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._m, tmpM, margenErrorLineaRecta);
            argInputFieldM.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpMError);

            var tmpBError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._b, tmpB, margenErrorLineaRecta);
            argInputFieldB.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpBError);

            if (!tmpMError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpBError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (tmpNotaMaxima >= 0.98f)
            {
                imageResultado.color = Color.green;
                argLineRender.material = refPlanoCartesiano._materialGraficaCorrecta;
            }
            else
            {
                imageResultado.color = Color.red;
                argLineRender.material = refPlanoCartesiano._materialGraficaIncorrecta;
            }

            Debug.Log("VerificarDatosRecta tmpNotaMaxima :" + tmpNotaMaxima.ToString("0.0000000"));
            return tmpNotaMaxima;
        }

        private float VerificarDatosParabola(LineRenderer argLineRender, InputField argInputFieldH, InputField argInputFieldK, InputField argInputFieldP)
        {
            var tmpNotaMaxima = 0f;
            var tmpFragmentoNota = 1f / 3f;

            var tmpFuncionAleatoriaSeleccionada = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada;

            var tmpH = ConvertirEscala(float.Parse(argInputFieldH.text));
            var tmpK = ConvertirEscala(float.Parse(argInputFieldK.text));
            var tmpP = ConvertirEscala(float.Parse(argInputFieldP.text));

            var tmpHError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._h, tmpH, margenErrorParabola);
            argInputFieldH.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpHError);

            var tmpKError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._k, tmpK, margenErrorParabola);
            argInputFieldK.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpKError);

            var tmpPError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._p, tmpP, margenErrorParabola);
            argInputFieldP.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpPError);

            if (!tmpHError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpKError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpPError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (tmpNotaMaxima >= 0.98f)
            {
                imageResultado.color = Color.green;
                argLineRender.material = refPlanoCartesiano._materialGraficaCorrecta;
            }
            else
            {
                imageResultado.color = Color.red;
                argLineRender.material = refPlanoCartesiano._materialGraficaIncorrecta;
            }

            Debug.Log("VerificarDatosParabola tmpNotaMaxima :" + tmpNotaMaxima.ToString("0.0000000"));
            return tmpNotaMaxima;
        }

        private float VerificarDatosCirfunferencia(LineRenderer argLineRender, InputField argInputFieldH, InputField argInputFieldK, InputField argInputFieldR)
        {
            var tmpNotaMaxima = 0f;
            var tmpFragmentoNota = 1f / 3f;

            var tmpFuncionAleatoriaSeleccionada = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaCircunferenciaSeleccionada;

            var tmpH = ConvertirEscala(float.Parse(argInputFieldH.text));
            var tmpK = ConvertirEscala(float.Parse(argInputFieldK.text));
            var tmpR = ConvertirEscala(float.Parse(argInputFieldR.text));

            var tmpHError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._h, tmpH, margenErrorCircunferencia);
            argInputFieldH.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpHError);

            var tmpKError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._k, tmpK, margenErrorCircunferencia);
            argInputFieldK.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpKError);

            var tmpRError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._r, tmpR, margenErrorCircunferencia);
            argInputFieldR.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpRError);

            if (!tmpHError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpKError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpRError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (tmpNotaMaxima >= 0.98f)
            {
                imageResultado.color = Color.green;
                argLineRender.material = refPlanoCartesiano._materialGraficaCorrecta;
            }
            else
            {
                argLineRender.material = refPlanoCartesiano._materialGraficaIncorrecta;
                imageResultado.color = Color.red;
            }

            Debug.Log("VerificarDatosCirfunferencia tmpNotaMaxima :" + tmpNotaMaxima.ToString("0.0000000"));
            return tmpNotaMaxima;
        }

        private float VerificarDatosElipse(LineRenderer argLineRender, InputField argInputFieldH, InputField argInputFieldK, InputField argInputFieldA, InputField argInputFieldB)
        {
            var tmpNotaMaxima = 0f;
            var tmpFragmentoNota = 1f / 4f;

            var tmpFuncionAleatoriaSeleccionada = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaElipseSeleccionada;

            var tmpH = ConvertirEscala(float.Parse(argInputFieldH.text));
            var tmpK = ConvertirEscala(float.Parse(argInputFieldK.text));
            var tmpA = ConvertirEscala(float.Parse(argInputFieldA.text));
            var tmpB = ConvertirEscala(float.Parse(argInputFieldB.text));

            var tmpHError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._h, tmpH, margenErrorElipse);
            argInputFieldH.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpHError);

            var tmpKError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._k, tmpK, margenErrorElipse);
            argInputFieldK.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpKError);

            var tmpAError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._a, tmpA, margenErrorElipse);
            argInputFieldA.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpAError);

            var tmpBError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._b, tmpB, margenErrorElipse);
            argInputFieldB.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpBError);

            if (!tmpHError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpKError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpAError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpBError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (tmpNotaMaxima >= 0.98f)
            {
                imageResultado.color = Color.green;
                argLineRender.material = refPlanoCartesiano._materialGraficaCorrecta;
            }
            else
            {
                imageResultado.color = Color.red;
                argLineRender.material = refPlanoCartesiano._materialGraficaIncorrecta;
            }

            Debug.Log("VerificarDatosElipse tmpNotaMaxima :" + tmpNotaMaxima.ToString("0.0000000"));
            return tmpNotaMaxima;
        }

        private float VerificarDatosHiperbola(LineRenderer argLineRender, InputField argInputFieldH, InputField argInputFieldK, InputField argInputFieldA, InputField argInputFieldB)
        {
            var tmpNotaMaxima = 0f;
            var tmpFragmentoNota = 1f / 4f;

            var tmpFuncionAleatoriaSeleccionada = refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada;

            var tmpH = ConvertirEscala(float.Parse(argInputFieldH.text));
            var tmpK = ConvertirEscala(float.Parse(argInputFieldK.text));
            var tmpA = ConvertirEscala(float.Parse(argInputFieldA.text));
            var tmpB = ConvertirEscala(float.Parse(argInputFieldB.text));

            var tmpHError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._h, tmpH, margenErrorHiperbola);
            argInputFieldH.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpHError);

            var tmpKError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._k, tmpK, margenErrorHiperbola);
            argInputFieldK.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpKError);

            var tmpAError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._a, tmpA, margenErrorHiperbola);
            argInputFieldA.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpAError);

            var tmpBError = GetErrorValor(tmpFuncionAleatoriaSeleccionada._b, tmpB, margenErrorHiperbola);
            argInputFieldB.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(tmpBError);

            if (!tmpHError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpKError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpAError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (!tmpBError)
                tmpNotaMaxima += tmpFragmentoNota;

            if (tmpNotaMaxima >= 0.98f)
            {
                imageResultado.color = Color.green;
                argLineRender.material = refPlanoCartesiano._materialGraficaCorrecta;
            }
            else
            {
                imageResultado.color = Color.red;
                argLineRender.material = refPlanoCartesiano._materialGraficaIncorrecta;
            }

            Debug.Log("VerificarDatosHiperbola tmpNotaMaxima :" + tmpNotaMaxima.ToString("0.0000000"));
            return tmpNotaMaxima;
        }

        private void SetIncorrectXOnInputsFields(InputField[] argInputField, bool argSetX)
        {
            foreach (var tmpInputField in argInputField)
            {
                tmpInputField.transform.Find("ImageValorFuncionIncorrecto").gameObject.SetActive(argSetX);
            }
        }

        private void SetFuncionSeleccionadaIncorrecta(bool argEsIncorrecta)
        {
            transform.Find("PanelContenedorListaFunciones").GetChild(indiceFuncionSeleccionada).Find("ImageFuncionSeleccionadaIncorrecta").gameObject.SetActive(argEsIncorrecta);
            if (argEsIncorrecta)
                cantidadErrores++;
        }

        /// <summary>
        /// Consigue todos los valores escritos en los inputs field
        /// </summary>
        /// <param name="argInputsFields"></param>
        /// <param name="argNombresImputsFields"></param>
        /// <returns>Array con los valores leidos de los input field</returns>
        private float[] GetValoresInputsFieldFuncionSeleccionada(out InputField[] argInputsFields, params string[] argNombresImputsFields)
        {
            float[] tmpValues = new float[argNombresImputsFields.Length];
            argInputsFields = new InputField[argNombresImputsFields.Length];

            var tmpInputsFuncionSeleccionada = transform.Find("PanelRegistroDatosInputs").GetChild(indiceFuncionSeleccionada + 1);

            for (int i = 0; i < argNombresImputsFields.Length; i++)
            {
                argInputsFields[i] = tmpInputsFuncionSeleccionada.Find(argNombresImputsFields[i]).GetChild(1).GetComponent<InputField>();
                var tmpTextInputField = argInputsFields[i].text;

                if (tmpTextInputField == string.Empty)
                {
                    tmpValues = null;
                    argInputsFields = null;
                    break;
                }
                else
                    tmpValues[i] = ConvertirEscala(float.Parse(tmpTextInputField));
            }

            return tmpValues;
        }

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        public void OnButtonFuncion(int argIndiceFuncionSeleccionada)
        {
            if (argIndiceFuncionSeleccionada != indiceFuncionSeleccionada)
            {
                indiceFuncionSeleccionadaTemporal = argIndiceFuncionSeleccionada;
                buttonElegir.interactable = true;
            }
            else
                buttonElegir.interactable = false;
        }

        public void OnButtonElegir()
        {
            activoBotonVerificar = true;
            if (ultimaFuncionVerificada)
                Destroy(ultimaFuncionVerificada);

            var tmpPanelContenedorListaFunciones = transform.Find("PanelContenedorListaFunciones");
            var tmpPanelRegistroDatosInputs = transform.Find("PanelRegistroDatosInputs");

            if (indiceFuncionSeleccionada != -1)
            {
                tmpPanelContenedorListaFunciones.GetChild(indiceFuncionSeleccionada).GetComponent<Button>().interactable = true;
                tmpPanelContenedorListaFunciones.GetChild(indiceFuncionSeleccionada).Find("ImageFuncionSeleccionadaIncorrecta").gameObject.SetActive(false);
                tmpPanelRegistroDatosInputs.GetChild(indiceFuncionSeleccionada + 1).gameObject.SetActive(false);
            }

            indiceFuncionSeleccionada = indiceFuncionSeleccionadaTemporal;
            tmpPanelContenedorListaFunciones.GetChild(indiceFuncionSeleccionada).GetComponent<Button>().interactable = false;
            buttonElegir.interactable = false;

            tmpPanelRegistroDatosInputs.GetChild(indiceFuncionSeleccionada + 1).gameObject.SetActive(true);

            foreach (var item in tmpPanelRegistroDatosInputs.GetChild(indiceFuncionSeleccionada + 1).GetComponentsInChildren<InputField>())
                item.text = "0";
        }

        public void OnButtonVerificar()
        {
            var tmpInputsFuncionSeleccionada = transform.Find("PanelRegistroDatosInputs").GetChild(indiceFuncionSeleccionada + 1);
            cantidadErrores = 0;

            float[] tmpValoresInputsFieldFuncion;
            InputField[] tmpInputsFieldsFuncion;
            LineRenderer tmpLineRenderFuncionCreada;

            Destroy(ultimaFuncionVerificada);

            switch (indiceFuncionSeleccionada)
            {
                case 0:
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputR");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    SetFuncionSeleccionadaIncorrecta(true);
                    imageResultado.color = Color.red;
                    SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);

                    var tmpFuncionRectaSinPendiente = refPlanoCartesiano.CrearFuncionRectaSinPendiente(tmpValoresInputsFieldFuncion[0], transformSistemaSolar);
                    tmpFuncionRectaSinPendiente.CrearLineRenderer("LineRendererFuncionRectaSinPendiente", refPlanoCartesiano._materialGraficaIncorrecta, tmpFuncionRectaSinPendiente.transform);
                    tmpFuncionRectaSinPendiente.SetPositionLineRenderer(tmpFuncionRectaSinPendiente.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionRectaSinPendiente.gameObject;

                    break;

                case 1://circunferencia                 
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputR");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionCircunferencia = refPlanoCartesiano.CrearFuncionCircunferencia(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], transformSistemaSolar);
                    tmpLineRenderFuncionCreada = tmpFuncionCircunferencia.CrearLineRenderer("LineRendererFuncionCircunferencia", null, tmpFuncionCircunferencia.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.circunferencia)
                        VerificarDatosCirfunferencia(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2]);
                    else
                    {
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        SetFuncionSeleccionadaIncorrecta(true);
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionCircunferencia.SetPositionLineRenderer(tmpFuncionCircunferencia.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionCircunferencia.gameObject;
                    break;

                case 2://cubica
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputP");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    SetFuncionSeleccionadaIncorrecta(true);
                    imageResultado.color = Color.red;
                    SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);

                    var tmpFuncionCubica = refPlanoCartesiano.CrearFuncionCubica(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], transformSistemaSolar);
                    tmpFuncionCubica.CrearLineRenderer("LineRendererFuncionCubica", refPlanoCartesiano._materialGraficaIncorrecta, tmpFuncionCubica.transform);
                    tmpFuncionCubica.SetPositionLineRenderer(tmpFuncionCubica.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionCubica.gameObject;
                    break;

                case 3://Recta pendiente
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputM", "PanelInputB");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionRectaPendiente = refPlanoCartesiano.CrearFuncionRectaPendiente(tmpValoresInputsFieldFuncion[0] * 100, tmpValoresInputsFieldFuncion[1], transformSistemaSolar);
                    tmpLineRenderFuncionCreada = tmpFuncionRectaPendiente.CrearLineRenderer("LineRendererFuncionRectaPendiente", null, tmpFuncionRectaPendiente.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.recta)
                        VerificarDatosRecta(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1]);
                    else
                    {
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        SetFuncionSeleccionadaIncorrecta(true);
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionRectaPendiente.SetPositionLineRenderer(tmpFuncionRectaPendiente.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionRectaPendiente.gameObject;
                    break;

                case 4://Funcion campana
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputR");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    SetFuncionSeleccionadaIncorrecta(true);
                    imageResultado.color = Color.red;
                    SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);

                    var tmpFuncionCampana = refPlanoCartesiano.CrearFuncionCampana(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], transformSistemaSolar);
                    tmpFuncionCampana.CrearLineRenderer("LineRendererFuncionCampana", refPlanoCartesiano._materialGraficaIncorrecta, tmpFuncionCampana.transform);
                    tmpFuncionCampana.SetPositionLineRenderer(tmpFuncionCampana.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionCampana.gameObject;
                    break;

                case 5://Parabola horizontal
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputP");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionParabolaVertical = refPlanoCartesiano.CrearFuncionParabola(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], transformSistemaSolar, DireccionParabola.derecha);
                    tmpLineRenderFuncionCreada = tmpFuncionParabolaVertical.CrearLineRenderer("LineRendererParabolaVertical", null, tmpFuncionParabolaVertical.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.parabola)
                    {
                        if (refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._direccionParabola == DireccionParabola.derecha || refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._direccionParabola == DireccionParabola.izquierda)
                            VerificarDatosParabola(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2]);
                        else
                        {
                            tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                            SetFuncionSeleccionadaIncorrecta(true);
                            imageResultado.color = Color.red;
                            SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                        }
                    }
                    else
                    {
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        SetFuncionSeleccionadaIncorrecta(true);
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionParabolaVertical.SetPositionLineRenderer(tmpFuncionParabolaVertical.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionParabolaVertical.gameObject;

                    break;

                case 6://parabola vertical
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputP");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionParabolaHorizontal = refPlanoCartesiano.CrearFuncionParabola(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], transformSistemaSolar, DireccionParabola.arriba);
                    tmpLineRenderFuncionCreada = tmpFuncionParabolaHorizontal.CrearLineRenderer("LineRendererParabolaHorizontal", null, tmpFuncionParabolaHorizontal.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.parabola)
                    {
                        if (refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._direccionParabola == DireccionParabola.arriba || refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaParabolaSeleccionada._direccionParabola == DireccionParabola.abajo)
                            VerificarDatosParabola(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2]);
                        else
                        {
                            tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                            SetFuncionSeleccionadaIncorrecta(true);
                            imageResultado.color = Color.red;
                            SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                        }
                    }
                    else
                    {
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        SetFuncionSeleccionadaIncorrecta(true);
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionParabolaHorizontal.SetPositionLineRenderer(tmpFuncionParabolaHorizontal.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionParabolaHorizontal.gameObject;
                    break;

                case 7://elipse 
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputA", "PanelInputB");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionElipse = refPlanoCartesiano.CrearFuncionElipse(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], tmpValoresInputsFieldFuncion[3], transformSistemaSolar);
                    tmpLineRenderFuncionCreada = tmpFuncionElipse.CrearLineRenderer("LineRendererFuncionElipse", null, tmpFuncionElipse.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.elipse)
                        VerificarDatosElipse(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2], tmpInputsFieldsFuncion[3]);
                    else
                    {
                        SetFuncionSeleccionadaIncorrecta(true);
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionElipse.SetPositionLineRenderer(tmpFuncionElipse.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionElipse.gameObject;
                    break;

                case 8://Hiperbola horizontal
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputA", "PanelInputB");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionHiperbolaHorizontal = refPlanoCartesiano.CrearFuncionHiperbola(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], tmpValoresInputsFieldFuncion[3], transformSistemaSolar, SentidoHiperbola.horizontal);
                    tmpLineRenderFuncionCreada = tmpFuncionHiperbolaHorizontal.CrearLineRenderer("LineRendererFuncionHiperbolaHorizontal", null, tmpFuncionHiperbolaHorizontal.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.hiperbola)
                    {
                        if (refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._sentidoHiperbola == SentidoHiperbola.horizontal)
                            VerificarDatosHiperbola(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2], tmpInputsFieldsFuncion[3]);
                        else
                        {
                            SetFuncionSeleccionadaIncorrecta(true);
                            tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                            imageResultado.color = Color.red;
                            SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                        }
                    }
                    else
                    {
                        SetFuncionSeleccionadaIncorrecta(true);
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionHiperbolaHorizontal.SetPositionLineRenderer(tmpFuncionHiperbolaHorizontal.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionHiperbolaHorizontal.gameObject;
                    break;

                case 9://Hiperbola vertical
                    tmpValoresInputsFieldFuncion = GetValoresInputsFieldFuncionSeleccionada(out tmpInputsFieldsFuncion, "PanelInputH", "PanelInputK", "PanelInputA", "PanelInputB");

                    if (tmpValoresInputsFieldFuncion == null)
                    {
                        refBoxMessageManager.MtdCreateBoxMessageInfo("Porfavor rellene todos los valores de la funcion seleccionada para poder verificar", "Aceptar");
                        return;
                    }

                    var tmpFuncionHiperbolaVertical = refPlanoCartesiano.CrearFuncionHiperbola(tmpValoresInputsFieldFuncion[0], tmpValoresInputsFieldFuncion[1], tmpValoresInputsFieldFuncion[2], tmpValoresInputsFieldFuncion[3], transformSistemaSolar, SentidoHiperbola.vertical);
                    tmpLineRenderFuncionCreada = tmpFuncionHiperbolaVertical.CrearLineRenderer("LineRendererFuncionHiperbolaVertical", null, tmpFuncionHiperbolaVertical.transform);

                    if (refPanelInterfazBotonesSeleccionSituacion._funcionSeleccionada == SituacionSeleccionada.hiperbola)
                    {
                        if (refPanelInterfazBotonesSeleccionSituacion._funcionAleatoriaHiperbolaSeleccionada._sentidoHiperbola == SentidoHiperbola.vertical)
                            VerificarDatosHiperbola(tmpLineRenderFuncionCreada, tmpInputsFieldsFuncion[0], tmpInputsFieldsFuncion[1], tmpInputsFieldsFuncion[2], tmpInputsFieldsFuncion[3]);
                        else
                        {
                            SetFuncionSeleccionadaIncorrecta(true);
                            tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                            imageResultado.color = Color.red;
                            SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                        }
                    }
                    else
                    {
                        SetFuncionSeleccionadaIncorrecta(true);
                        tmpLineRenderFuncionCreada.material = refPlanoCartesiano._materialGraficaIncorrecta;
                        imageResultado.color = Color.red;
                        SetIncorrectXOnInputsFields(tmpInputsFieldsFuncion, true);
                    }

                    tmpFuncionHiperbolaVertical.SetPositionLineRenderer(tmpFuncionHiperbolaVertical.GetCoordenadasXYZ(), false);
                    ultimaFuncionVerificada = tmpFuncionHiperbolaVertical.gameObject;
                    break;
            }

            ActivarPanel(false);
            refPanelInterfazBotonesIzquierda.SetSeleccionYDatosFuncionCorrecta(cantidadErrores == 0);
            refControladorValoresPDF.SetInputsFuncionSeleccionada(transform.Find("PanelRegistroDatosInputs").GetChild(indiceFuncionSeleccionada + 1).gameObject);
            refControladorValoresPDF.SetSpriteFuncionSeleccionada(transform.Find("PanelContenedorListaFunciones").GetChild(indiceFuncionSeleccionada).GetChild(0).gameObject, transform.Find("PanelContenedorListaFunciones").GetChild(indiceFuncionSeleccionada).GetChild(1).gameObject);


            if (cantidadErrores > 0)
            {
                refEvaluacion.AsignarFuncionGraficadoCorrecta(0);
                refControladorDatosSesion.AddIntentos();
            }
            else
                refEvaluacion.AsignarFuncionGraficadoCorrecta(1);
        }

        public void SalirSituacion()
        {
            activoBotonVerificar = false;
            Destroy(ultimaFuncionVerificada);
            ResetearElementosUsados();
        }
        #endregion
    }
}