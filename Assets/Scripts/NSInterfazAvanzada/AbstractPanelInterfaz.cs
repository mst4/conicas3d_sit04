﻿using NSTraduccionIdiomas;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{
    /// <summary>
    /// Clase que debe ir adjunta a cada panel de la interfaz para controlar la animacion de aparicion
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class AbstractPanelInterfaz : MonoBehaviour
    {
        #region members
        [Header("Animación panel"), Space(20)]

        /// <summary>
        /// Curva de animacion de la escala del panel cuando la ventana se esta mostrando
        /// </summary>
        [SerializeField, Tooltip("Curva de animacion de la escala del panel cuando la ventana se esta mostrando")]
        private AnimationCurvePanelInterfaz animationCuveEscalaAparecer;

        /// <summary>
        /// Curva de animacion de la escala del panel cuando la ventana se esta ocultando
        /// </summary>
        [SerializeField, Tooltip("Curva de animacion de la escala del panel cuando la ventana se esta ocultando")]
        private AnimationCurvePanelInterfaz animationCuveEscalaOcultar;

        /// <summary>
        /// Curva de animacion de la transparencia del panel cuando se esta mostrando
        /// </summary>
        [SerializeField, Tooltip("Curva de animacion de la transparencia del panel cuando se esta mostrando")]
        private AnimationCurvePanelInterfaz animationCuveTransparenciaAparecer;

        /// <summary>
        /// Curva de animacion de la transparencia del panel cuando se esta ocultando
        /// </summary>
        [SerializeField, Tooltip("Curva de animacion de la transparencia del panel cuando se esta ocultando")]
        private AnimationCurvePanelInterfaz animationCuveTransparenciaOcultar;

        /// <summary>
        /// Para controlar la transparencia del panel
        /// </summary>
        private CanvasGroup canvasGroup;

        [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe aparecer por completo")]
        /// <summary>
        /// Tiempo en segundos en el que el panel debe aparecer por completo
        /// </summary>
        private float tiempoAparicion;

        [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe ocultarse por completo")]
        /// <summary>
        /// Tiempo en segundos en el que el panel debe ocultarse por completo
        /// </summary>
        private float tiempoOcultacion;

        /// <summary>
        /// Ejecutar evento cuando el panel se oculta?
        /// </summary>
        [SerializeField]
        private bool ejecutarEventoOcultacion;

        private IEnumerator couAparecer;

        private IEnumerator couOcultar;

        [SerializeField, Header("Panel fondo oscuro")]
        private GameObject prefabPanelImagenFondoVentanaObscura;

        private GameObject panelImagenFondoActivo;

        [SerializeField]
        private bool crearImagenFondoOscuro;
        #endregion

        #region events

        [Header("Eventos"), Space(20)]

        /// <summary>
        /// Se ejecuta cuando el panel completa su aparicion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su aparicion")]
        private UnityEvent OnPanelAparecio;

        /// <summary>
        /// Se ejecuta cuando el panel completa su ocultacion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su ocultacion")]
        private UnityEvent OnPanelOculto;
        #endregion

        #region public methods

        /// <summary>
        /// Metodo abstracto que activa el panel o lo desactiva, se usa este metodo abstracto para poder definir siempre alguna accion en la condicion de activacion
        /// </summary>
        /// <param name="argActivar">Activar panel?</param>
        public abstract void ActivarPanel(bool argActivar = true);

        /// <summary>
        /// Asigna el bool que permite que se pueda ejecutar el evento que notifica que debe ejecutarse el evento de ocultacion
        /// </summary>
        /// <param name="argEjecutarEvento">Ejecutar el evento de ocultacion? </param>
        public void EjecutarEventoOcultacion(bool argEjecutarEvento)
        {
            ejecutarEventoOcultacion = argEjecutarEvento;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Ejecuta la animacion del panel para este aparesca o se oculte
        /// </summary>
        /// <param name="argMostrar">Mostrar el panel? o ocultarlo?</param>
        protected void Mostrar(bool argMostrar = true)
        {
            if (canvasGroup == null)
                canvasGroup = GetComponent<CanvasGroup>();

            if (argMostrar)
            {
                if (couAparecer == null)
                {
                    gameObject.SetActive(true);
                    StopAllCoroutines();
                    couOcultar = null;
                    couAparecer = CouAparecer();
                    StartCoroutine(couAparecer);

                    if (crearImagenFondoOscuro)
                    {
                        panelImagenFondoActivo = Instantiate(prefabPanelImagenFondoVentanaObscura, transform.parent);
                        panelImagenFondoActivo.GetComponent<Transform>().SetSiblingIndex(transform.GetSiblingIndex());
                    }
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para aparecer");
            }
            else
            {
                if (couOcultar == null)
                {
                    StopAllCoroutines();
                    couAparecer = null;
                    couOcultar = CouOcultar();

                    if (gameObject.activeSelf)
                        StartCoroutine(CouOcultar());

                    if (crearImagenFondoOscuro)
                        if (panelImagenFondoActivo)
                            Destroy(panelImagenFondoActivo);
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para ocultarse");
            }
        }
        #endregion

        #region courutines

        /// <summary>
        /// Courutina que ejecuta la animacion que hace aparecer el panel
        /// </summary>
        private IEnumerator CouAparecer()
        {
            var tmpTiempoAnimacion = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();

            while (tmpTiempoAnimacion <= tiempoAparicion)
            {
                tmpTiempoAnimacion += Time.deltaTime;
                var tmpTiempoMaximoCurvaAnimacionEscala = animationCuveEscalaAparecer._animationCurve.keys[animationCuveEscalaAparecer._animationCurve.keys.Length - 1].time;
                tmpRectTransform.localScale = Vector3.one * animationCuveEscalaAparecer._animationCurve.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoAparicion);
                var tmpTiempoMaximoCurvaAnimacionAlpha = animationCuveTransparenciaAparecer._animationCurve.keys[animationCuveTransparenciaAparecer._animationCurve.keys.Length - 1].time;
                canvasGroup.alpha = animationCuveTransparenciaAparecer._animationCurve.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoAparicion);
                yield return null;
            }

            OnPanelAparecio.Invoke();
            couAparecer = null;
        }

        /// <summary>
        /// Courutina que ejecuta la animacion que hace ocultar el panel
        /// </summary>
        private IEnumerator CouOcultar()
        {
            var tmpTiempoAnimacion = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();

            while (tmpTiempoAnimacion <= tiempoOcultacion)
            {
                tmpTiempoAnimacion += Time.deltaTime;
                var tmpTiempoMaximoCurvaAnimacionEscala = animationCuveEscalaOcultar._animationCurve.keys[animationCuveEscalaOcultar._animationCurve.keys.Length - 1].time;
                tmpRectTransform.localScale = Vector3.one * animationCuveEscalaOcultar._animationCurve.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoOcultacion);
                var tmpTiempoMaximoCurvaAnimacionAlpha = animationCuveTransparenciaOcultar._animationCurve.keys[animationCuveTransparenciaOcultar._animationCurve.keys.Length - 1].time;
                canvasGroup.alpha = animationCuveTransparenciaOcultar._animationCurve.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoOcultacion);
                yield return null;
            }

            gameObject.SetActive(false);

            if (ejecutarEventoOcultacion)
                OnPanelOculto.Invoke();

            couOcultar = null;
        }


        #endregion
    }
}