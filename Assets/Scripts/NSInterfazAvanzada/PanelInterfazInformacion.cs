﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada 
{
	public class PanelInterfazInformacion : AbstractPanelInterfaz
	{

        #region members
        public NSInterfazAvanzada.PanelInterfazBotonesSeleccionSituacion situasionseleccionada;

        public GameObject InformacionRecta;
        public GameObject InformacionCircunferencia;
        public GameObject InformacionElipse;
        public GameObject InformacionHiperbole;
        public GameObject InformacionParabola;

        public Toggle sutuacion;
        public Toggle practica;
        public Toggle ecuacion;

        private GameObject panelDeInfromacionActivo;


        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{
            //panelDeInfromacionActivo= InformacionRecta;
        }

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);

            InformacionRecta.SetActive(false);
            InformacionParabola.SetActive(false);
            InformacionCircunferencia.SetActive(false);
            InformacionElipse.SetActive(false);
            InformacionHiperbole.SetActive(false);

            switch (situasionseleccionada._funcionSeleccionada)
            {
                
                case SituacionSeleccionada.recta:
                    panelDeInfromacionActivo = InformacionRecta;
                    InformacionRecta.SetActive(true);
                    InformacionParabola.SetActive(false);
                    InformacionCircunferencia.SetActive(false);
                    InformacionElipse.SetActive(false);
                    InformacionHiperbole.SetActive(false);

                    break;
                case SituacionSeleccionada.parabola:
                    panelDeInfromacionActivo = InformacionParabola;
                    InformacionParabola.SetActive(true);
                    InformacionRecta.SetActive(false);
                    InformacionCircunferencia.SetActive(false);
                    InformacionElipse.SetActive(false);
                    InformacionHiperbole.SetActive(false);
                    break;
                case SituacionSeleccionada.circunferencia:
                    panelDeInfromacionActivo = InformacionCircunferencia;
                    InformacionCircunferencia.SetActive(true);
                    InformacionRecta.SetActive(false);
                    InformacionParabola.SetActive(false);
                    InformacionElipse.SetActive(false);
                    InformacionHiperbole.SetActive(false);

                    break;
                case SituacionSeleccionada.elipse:
                    panelDeInfromacionActivo = InformacionElipse;
                    InformacionElipse.SetActive(true);
                    InformacionRecta.SetActive(false);
                    InformacionParabola.SetActive(false);
                    InformacionCircunferencia.SetActive(false);
                    InformacionHiperbole.SetActive(false);

                    break;
                case SituacionSeleccionada.hiperbola:
                    panelDeInfromacionActivo = InformacionHiperbole;
                    InformacionHiperbole.SetActive(true);
                    InformacionRecta.SetActive(false);
                    InformacionParabola.SetActive(false);
                    InformacionCircunferencia.SetActive(false);
                    InformacionElipse.SetActive(false);
                    break;
            }


        }

        public void mtdReiniciar()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarSituacion();
            sutuacion.isOn=true;
            practica.isOn= false;
            ecuacion.isOn=false;
        }

        public void MtdActivarInfoSituacionDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarSituacion();
        }

        public void MtdActivarInfoEcuacionesDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarEcuaciones ();
        }

        public void MtdActivarInfoProcedimientoDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarProcedimeinto ();
        }

        #endregion

        #region courutines

        #endregion

    }
}