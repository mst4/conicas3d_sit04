﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSObjectSelector
{
    public interface IObjectSelectable
    {

        #region public methods

        void SetSelected(bool argSelected);

        bool GetIsDraggable();

        /// <summary>
        /// Asigna la posicion actual de arrastre
        /// </summary>
        /// <param name="argPosition">Posicion del mouse sobre el plano proyectado por el selector de objetos</param>
        /// <param name="argOffset">Distancia desde el mouse al centro del objeto seleccionado</param>
        void SetDraggedPosition(Vector3 argPosition, Vector3 argOffset);

 		#endregion
	}
}