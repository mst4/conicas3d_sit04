﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSTraduccionIdiomas
{
	public class TextParaTraducir : Text
	{
        #region members

        private DiccionarioIdiomas diccionarioIdiomas;

        private string textoOriginalEspaniol;
        #endregion

        #region accesors

        public string _textoOriginalEspaniol
        {
            set
            {
                textoOriginalEspaniol = value;
                text = value;

                if (!diccionarioIdiomas)
                    diccionarioIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();

                diccionarioIdiomas.Traducir(this.name, this.text);
            }
            get
            {
                return textoOriginalEspaniol;
            }
            
        }

        #endregion

        #region monoBehaviour

        protected override void Awake()
        {
            base.Awake();

                       
        }

        protected override void Start()
        {
            base.Start();
            if (!diccionarioIdiomas)
                diccionarioIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();

            textoOriginalEspaniol = text;
        }


        protected override void OnEnable()
        {
            base.OnEnable();

            if (diccionarioIdiomas != null)
            {
                diccionarioIdiomas.Traducir(this);
            }
        }

        #endregion
    }
}