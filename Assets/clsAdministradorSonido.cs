﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clsAdministradorSonido : MonoBehaviour {

    public float Volpuerta;
    public AudioSource SonidoDeFondo;
    public AudioSource puerta1;
    public AudioSource puerta2;
    public AudioClip AbrirPuerta;
    public AudioClip CerrarPuerta;
    public AudioSource EntrarConsola;
    public AudioSource SalirConsola;
    public AudioSource tomarfoto;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void activarSonidoPuerta1Abrir()
    {
        puerta1.PlayOneShot(AbrirPuerta, Volpuerta);
    }


    public void activarSonidoPuerta1Cerrar()
    {
        puerta1.PlayOneShot(CerrarPuerta, Volpuerta);
    }


    public void activarSonidoPuerta2Abrir()
    {
        puerta2.PlayOneShot(AbrirPuerta, Volpuerta);
    }


    public void activarSonidoPuerta2Cerrar()
    {
        puerta2.PlayOneShot(CerrarPuerta, Volpuerta);
    }


    public void activarSOnidoConsola()
    {
        EntrarConsola.Play();
    }
    public void activarSalirConsola()
    {
        SalirConsola.Play();
    }

    public void TomarfotoSounds()
    {
        tomarfoto.Play();
    }



}
